User.create!(
  name:  "Denesh V T",
  email: "thangadenesh123@gmail.com",
  password:              "denesh",
  password_confirmation: "denesh",
  admin: true,
  activated:    true ,
  activated_at: Time.zone.now
)

99.times do |n|
  name =  Faker::Name.name
  email = "example-#{n+1}@railstutorial.org"
  password = "password"
  User.create!(
    name: name,
    email: email,
    password:              password,
    password_confirmation: password,
    activated:    true ,
    activated_at: Time.zone.now
  )
end

# Generating microposts for a subset of users.
users = User.order(:created_at).first(6)
50.times do
  content = Faker::Lorem.sentence(word_count: 5)
  users.each { |user| user.microposts.create!(content: content) }
end

# Following relationships
users = User.all
user  = User.first
following = users[2..50]
followers = users[3..80]
following.each { |followed| user.follow(followed) }
followers.each { |follower| follower.follow(user) }